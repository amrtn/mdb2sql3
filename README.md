﻿# mdb2sq3

## Project Description

This project aims to create a small application to convert a MSAccess Database file into SQLite format.
It needs the SQLite ADO library in order to work.
I do it just for learning purposes and for fun, of course :)

```
mdb2sq3: Converts a simple MSAccess Database file into a SQLite database.
usage:
        mdb2sq3 -s:sourcefile [-t:targetfile] [options]
            -s:sourcefile   Sets the source MSAccess database
            -t:targetfile   Sets the target SQLite database
            -p:password     MDB password (if protected)
            -tp:password	SQLite target password (optional)
            -e              Forces deletion of target file if it exists
            -v              Verbose mode.
            -?              Prints this help and exits the program.
``` 
    

## Origin

This project is a fork of <http://mdb2sq3.codeplex.com/> by [mjfernandez](http://www.codeplex.com/site/users/view/mjfernandez)